# README #

Contains a collection of scripts to generate custom overwatch statistics by scraping various website.
You will need to run each of the scripts to generate your own database of data which can be used to generate stats.
There are currently no scripts to plot stats in this repository.

### Scripts included ###

* scrape_overbuff.py
* owapi_json.py
* pickle_combine.py
* pickle_histo.py

### scrape_overbuff.py ###

This script will scrape overbuff for a corpus of search terms specified as an input file with one search term per-line.
Only battletags with an SR ranking are saved into a dictionary.
The dictionary is output as a pickle file as the result of this script.
This assumes a specific HTML layout on the overbuff website which is subject to change at any time.

### owapi_json.py ###

This is meant to be run locally as owapi.net WILL rate limit you.
Downloads json formatted data for a battletag.
This data was scraped from playoverwatch.com, however pulling from this 3rd party site is more convenient.
It takes the pickle file from scrape_overbuff.py as input and outputs json files to the specific output directory.
This collection of json files is your personal database of overwatch stats.

### playow_html.py ###

Downloads raw html formatted data from a battletag directly from playoverwatch.com.
Really there should be no reason to use this unless 3rd party scrapers are no longer being updated.
This can be parsed to grab all of the data you would noramlly see on playoverwatch.com.
The input is a pickle file with battletags and the output parameter is a directory to write html files.

### pickle_combine.py ###

Combines multiple pickle files into one file.
This is provided for multiple scrapes of overbuff.
The input is a set of comma delimited pickle filenames.
Output is a single pickle filename.
Provided for convenience since the owapi_json.py script only accepts a single pickle file.

### picke_histo.py ###

Plots a histogram of a pickle file.
This shows the distribution of rankings which can be used as a sanity check to verify confidence in the statistics being calculated.
The distribution can be compared with the rankings distribution on masteroverwatch.com.
