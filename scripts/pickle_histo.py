import numpy as np
import matplotlib.pyplot as plt
import pickle
import optparse

parser = optparse.OptionParser()

# Note: The newer sessions should be LAST in the list
parser.add_option("--input", type="string", default="combined.pickle",
                  help="Pickle filename with accounts/SR")
parser.add_option("--output", type="string", default=None,
                  help="Output png filename")
parser.add_option("--show", action="store_true", default=False,
                  help="Show the plot before exiting")

(options, args) = parser.parse_args()

with open(options.input, 'rb') as handle:
    bnet_dict = pickle.loads(handle.read())

rankings = []
for rank in bnet_dict.itervalues():
    rankings.append(rank)

hist, bins = np.histogram(rankings, bins=100, range=(0.0, 5000.0))
width = 0.8 * (bins[1] - bins[0])
center = (bins[:-1] + bins[1:]) / 2
plt.bar(center, hist, align='center', width=width)
if options.show:
    plt.show()

if options.output != None:
    fig, ax = plt.subplots()
    ax.bar(center, hist, align='center', width=width)
    fig.savefig(options.output)
