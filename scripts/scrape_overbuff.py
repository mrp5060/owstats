from lxml import html
from lxml import etree
from time import sleep # to prevent throttles
import requests
import pickle
import optparse
import sys

parser = optparse.OptionParser()

# Note: The newer sessions should be LAST in the list
parser.add_option("--input", type="string", default="",
                  help="Filename with list of search names")
parser.add_option("--output", type="string", default="session.pickle",
                  help="Name of file to output combined data")

(options, args) = parser.parse_args()

search_names = []
bnet_dict = {}

# In case there is throttling going on, use exponential backoff
sleep_time = 0.1
sleep_max = 2
sleep_min = 0.1

# If OB is down, give up eventually
failure_max = 10
failure_count = 0

if options.input == None:
    print "No input file."
    sys.exit(1)

with open(options.input) as namefile:
    for line in namefile:
        name = line.rstrip()
        if len(name) > 2:
            search_names.append(name)

print "Searching %d names" % len(search_names)

search_count = 0
for name in search_names:
    print "Searching %s (%d/%d)... " % (name, search_count, len(search_names)), 
    page = requests.get('https://www.overbuff.com/search?q=%s' % name)
    if page.status_code != 200:
        print "not 200!"
        sleep(sleep_time)
        if sleep_time < sleep_max:
            sleep_time *= 2
        else:
            failure_count += 1
            if failure_count > failure_max:
                print "Reached max failure rate, quiting."
                break

        # Lazy route: skip to next name instead of retrying
        search_count += 1
        continue

    if sleep_time > sleep_min:
        sleep_time /= 2

    print "Parsing"

    tree = html.fromstring(page.content)

    results = tree.xpath('//div[@class="result"]')
    if len(results) == 0:
        search_count += 1
        continue

    try:
        for result in results:
            # Get the battle tag
            name_div = result.xpath('.//div[@class="name"]')

            # Should only be one name div 
            if len(avatar) != 1:
                raise

            anchor = name_div[0].xpath('.//a')

            # Should only be one anchor link
            if len(anchor) != 1:
                raise

            profile_link = anchor[0].get('href')

            # Get SR value
            rating = result.xpath('.//dd[@class="color-stat-rating"]')
            if len(rating) > 0:
                sr = int(rating[0].text)

                bnet_dict[profile_link] = sr

    except:
        print "Error"
        # Count this as a failure in case the html format changes
        failure_count += 1
        pass

    search_count += 1
    sleep(sleep_time)

print "Writing %s" % options.output
with open(options.output, 'wb') as handle:
    pickle.dump(bnet_dict, handle)
