import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import Polygon

data1 = np.random.rand(50) * 100
data2 = np.random.rand(50) * 100

fig, ax1 = plt.subplots()
fig.canvas.set_window_title('Performance Plot')

bp = plt.boxplot((data1, data2), vert=False, usermedians=[15,65], labels=["1", "2"])
plt.setp(bp['medians'], lw=5, color='darkred')

ax1.set_title('Player Name')

for line in bp['boxes']:
    x, y = line.get_xydata()[0] # right side of box
    plt.text(x, y, '%.1f%%  ' % x, horizontalalignment='right', verticalalignment='baseline')
    x, y = line.get_xydata()[3] # left side of box
    plt.text(x, y, '  %.1f%%' % x, horizontalalignment='left', verticalalignment='baseline')

    lineX = []
    lineY = []
    for j in range(5):
        lineX.append(line.get_xdata()[j])
        lineY.append(line.get_ydata()[j])
    lineCoords = list(zip(lineX, lineY))

    linePoly = Polygon(lineCoords, facecolor='royalblue')
    ax1.add_patch(linePoly)

for median in bp['medians']:
    ys = median.get_ydata()
    newy = [ys[0] * 0.99, ys[1] * 1.01]
    median.set_ydata(newy)
    #plt.setp(median, ydata=ys)

ax1.set_xlim(-10, 110)

plt.show()
