import requests
import urllib
import pickle
import optparse
import sys
from time import sleep

parser = optparse.OptionParser()

# Note: The newer sessions should be LAST in the list
parser.add_option("--input", type="string", default=None,
                  help="Pickle filename with bnet tags")
parser.add_option("--output", type="string", default="./",
                  help="Directory to output files to")
parser.add_option("--host", type="string", default="localhost",
                  help="URL of OWAPI server")
parser.add_option("--port", type="int", default=4444,
                  help="Port number of OWAPI server")

(options, args) = parser.parse_args()

failure_count = 0
failure_max = 10

sleep_time = 1
sleep_max = 16
sleep_min = 1

if options.input == None:
    print "No input file."
    sys.exit(1)

with open(options.input, 'rb') as handle:
    bnet_dict = pickle.loads(handle.read())

name_count = 1
for name in bnet_dict.iterkeys():
    print "%s (%d/%d)" % (name, name_count, len(bnet_dict))

    # Name format is /players/<platform>/<id>
    tokens = name.split('/')
    platform = tokens[2]
    btag = tokens[3]

    url = "https://%s:%d/api/v3/u/%s/blob?platform=%s" \
            % (options.host, options.port, urllib.quote(btag), urllib.quote(platform))
    page = requests.get(url)
    if page.status_code != 200:
        print "Failed to fetch %s" % btag
        print "Status code: %d" % page.status_code
        print page.content
        failure_count += 1

        if sleep_time < sleep_max:
            sleep_time *= 2
        
        if failure_count > failure_max:
            break

        continue

    # Note: timestamps could be used to treat multiple fetches of the same
    # battletags stats as different data points.
    filename = "%s_%s.json" % (platform, btag.replace(" ", "_"))
    output = "%s/%s" % (options.output, filename)
    with open(output, 'wb') as handle:
        handle.write(page.content)

    if sleep_time > sleep_min:
        sleep_time /= 2

    name_count += 1

    if name_count > 10:
        sys.exit(1)
