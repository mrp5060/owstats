import pickle
import optparse

parser = optparse.OptionParser()

# Note: The newer sessions should be LAST in the list
parser.add_option("--sessions", type="string",
                  help="Comma delimited list of sessions to combine")
parser.add_option("--output", type="string", default="combined.pickle",
                  help="Name of file to output combined data")

(options, args) = parser.parse_args()

master_dict = {}

for session in options.sessions.split(','):
    with open(session, 'rb') as handle:
        sub_dict = pickle.loads(handle.read())

    for key, value in sub_dict.iteritems():
        master_dict[key] = value

with open(options.output, 'wb') as handle:
    pickle.dump(master_dict, handle)

print "%d combined names" % len(master_dict)
